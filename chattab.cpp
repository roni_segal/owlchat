#include "chattab.h"

ChatTab::ChatTab(const Device* dev, QWidget *parent) : QWidget(parent)
{
    _dev = dev;



    _chatboxEdit = new QTextEdit(tr("connecting..."));
    _chatboxEdit->setReadOnly(true);
    if(Manager::getManager().connect(*_dev) == 0)
    {
        _chatboxEdit->append("\nERROR connecting to client");
    }
    _senderEdit = new QLineEdit("");

    // when enter is pressed in the sender it will call sendTxt() funtion
    connect(_senderEdit, SIGNAL(returnPressed()), this, SLOT(sendTxt()));

    //layouts
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(_chatboxEdit);
    mainLayout->addWidget(_senderEdit);
    setLayout(mainLayout);
}

Device ChatTab::getDev()
{
    return *_dev;
}

void ChatTab::recved(Message msg)
{
    _mu_chatboxEdit.lock();
    _chatboxEdit->append("\n" + QString::fromStdString(_dev->getName()) + QString::fromStdString(msg.getData()));
    _mu_chatboxEdit.unlock();
}

// sending the msg and add it the the chatbox
void ChatTab::sendTxt()
{
    // lock the chatbox so the recv and the send could not wirte it together
    _mu_chatboxEdit.lock();

    // add the msg to the chatbox
    _chatboxEdit->append("\n" + QString::fromStdString(Device::me()->getName()) + ": " + _senderEdit->text());
    //TODO: send the msg
    Manager::getManager().send(*_dev, Message(_senderEdit->text().toUtf8().constData()));

    _mu_chatboxEdit.unlock();
    _senderEdit->setText("");

    // scroll the chatbox to the end
    QScrollBar *sb = _chatboxEdit->verticalScrollBar();
    sb->setValue(sb->maximum());
}


