#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    Manager::getManager().setAppID("chat01");
    Manager::getManager().setName("Roni");
    Manager::getManager().setP2PCallback(p2pcallback, (void*)this);

    std::thread lookup_t(&MainWindow::lookupThread, this);
    lookup_t.detach();



    //exemp. to a new client in the chat
    //ui->tabs->addTab(new ChatTab(new Device("DevName", "DevMac")), QString("tab name"));
}

void MainWindow::lookupThread()
{
    while(true)
    {
        std::set<Device> devs = Manager::getManager().devicesLookup(50000);
        //remove from devs all old devices:
        for(int i = 1; i < ui->tabs->count(); i++)
        {
            Log::D("aaa");
            Device dev = ((ChatTab*)ui->tabs->widget(i))->getDev();
            Log::D("bbb");
            std::set<Device>::const_iterator it = devs.find(dev);
            Log::D("ccc");
            if(it != devs.end())
                devs.erase(it);
        }

        for(std::set<Device>::iterator it = devs.begin(); it != devs.end(); it++)
        {
            Log::D("a,,,,aa");
            ui->tabs->addTab(new ChatTab(&(*it)), QString::fromStdString(it->getName()));
        }
    }
}

void MainWindow::receiverThread(Device d)
{
    Log::D("started new thread, device name: " + d.getName());

    Message m = Manager::getManager().recv(d);

    //TODO: add tab mutex
    for(int i = 0; i < ui->tabs->count(); i++)
    {
        if(ui->tabs->tabText(i) == QString::fromStdString(d.getName()))
        {
            ChatTab* tab = (ChatTab*)ui->tabs->widget(i);
            tab->recved(m);
        }
    }
}

void MainWindow::p2pcallback(Device d, void* arg)
{
    MainWindow* ths = (MainWindow*)arg;
    Log::D("p2pcallback: d-" + d.getName());
    ths->_receiverThreads.push_back(std::thread(&MainWindow::receiverThread, ths, d));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_lineEdit_textChanged(const QString &arg1)
{


}

void MainWindow::on_lineEdit_returnPressed()
{
}
