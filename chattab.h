#ifndef CHATTAB_H
#define CHATTAB_H

#include <QWidget>
#include <QTextEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <string>
#include <QLineEdit>
#include <mutex>
#include <QScrollBar>

#include <open-wireless-library/Device.h>
#include <open-wireless-library/Manager.h>

class ChatTab : public QWidget
{
    Q_OBJECT
public:
    explicit ChatTab(const Device* dev, QWidget *parent = 0);

private:
    //void P2PCallback(Device* d);

    const Device* _dev;
    QTextEdit* _chatboxEdit;
    QLineEdit* _senderEdit;
    std::mutex _mu_chatboxEdit;

public:
    Device getDev();
    void recved(Message msg);

signals:

public slots:

private slots:
    void sendTxt();


};

#endif // CHATTAB_H
