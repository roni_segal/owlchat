#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <chattab.h>
#include <vector>
#include <thread>
#include <iostream>

#include <open-wireless-library/Log.h>

typedef void (*recvThread) (Device dev);

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    std::vector<std::thread> _receiverThreads;
    void receiverThread(Device d);

    ~MainWindow();

private:
    static void p2pcallback(Device d, void* arg);
    void lookupThread();
    Ui::MainWindow *ui;



private slots:
    void on_lineEdit_textChanged(const QString &arg1);

    void on_lineEdit_returnPressed();
};

#endif // MAINWINDOW_H
