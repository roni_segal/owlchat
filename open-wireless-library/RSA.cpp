#include "RSA.h"

RSA::RSA()
{
    srand(time(NULL));
	generate();
}
RSA::~RSA(){}

bool RSA::isPrime(RSA_KEY num)
{
	for (RSA_KEY i = 2; i < sqrtl(num) + 1; i++)
		if (!(num % i))
			return false;
	return true;
}

RSA_KEY RSA::randomPrime(int n)
{
	RSA_KEY a = powl(2, n), b = powl(2, n-1), x = 0;
    while(true)
    	if(isPrime(x = rand() % (a - b) + b))
    		return x;
    return 0;
}

int RSA::generate()
{
    int n = 12;
    _q = _p = randomPrime(n);
    while (_q == _p)
    	_q = randomPrime(n);
    _phi = (_p - 1) * (_q - 1);
    _n = _p * _q;
    _e = e();
    _d = d();
    return 0;
}

unsigned int RSA::e()
{
    for (unsigned int i = 2; i < _phi; i++)
        if ((_phi % i) && isPrime((RSA_KEY)i) && (i != _p) && (i != _q))
				return i;
	return 0;
}
RSA_KEY RSA::d()
{
	RSA_KEY k = 1;
	while (1)
	{
		k += _phi;
		if (k % _e == 0)
			return (k / _e);
	}
	return 0;
}

std::string RSA::encrypt(std::string s)
{
	return encrypt(s, _n, _e);
}

std::string RSA::decrypt(std::string s)
{
	return decrypt(s, _n, _d);
}

RSA_KEY RSA::cipher(RSA_KEY c, RSA_KEY n, RSA_KEY de)
{
	RSA_KEY x = 1;
	for(RSA_KEY i = 0; i < de; i ++)
			x = (x * c) % n;
	return x;
}

std::string RSA::encrypt(std::string s, RSA_KEY n, unsigned int e)
{
	std::string c = "";
	RSA_KEY x;
	for(unsigned int i = 0; i < s.length(); i ++)
	{
		x = RSA::cipher((RSA_KEY)(unsigned char)s[i], n, e);
		for(unsigned int j = 0; j < sizeof(RSA_KEY); j++)
			c.push_back(*((unsigned char *)(&x)+j));
	}
	return c;
}

std::string RSA::decrypt(std::string s, RSA_KEY n, RSA_KEY d)
{
	RSA_KEY x;
	std::string plain = "";
	for(unsigned int i = 0; i < s.length(); i += sizeof(RSA_KEY))
	{
		x = 0;
		for(unsigned int j = 0; j < sizeof(RSA_KEY); j++)
		{
			RSA_KEY y = 1;
			for(unsigned int k = 0; k < j; k++)
				y*=256;
			y*=(unsigned char)s[i+j];
			x = x + y;
		}
		plain.push_back(RSA::cipher(x, n, d));
	}
	return plain;
}
