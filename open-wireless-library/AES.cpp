#include "AES.h"

/****************************************************************************************
 * AES																					*
 * Advanced Encryption standard 														*
 * The AES class provides an implementation of the secured symmetric encryption system.	*
 * It contains functions for encrypting, decrypting and change the key.					*
 ****************************************************************************************/

AES::AES()
{
	//a 128-bit AES
	_nk = 4;
	_nr = 10;
	generateKey();
}
AES::AES(std::string key)
{
	_nk = 4;
	_nr = 10;
	_key = key;
}
AES::~AES(){}

void AES::generateKey()
{
	for(unsigned int i=0;i<_nk*4;i++)
		_key.push_back((unsigned char)(rand()%256));
}

std::string AES::encrypt(std::string p)
{
	while(p.length()%16)
		p.push_back('\0');
	unsigned char in[16];
	for(unsigned int j = 0; j < (p.length() / 16); j ++)
	{
		for(unsigned int i = 0; i < 16; i ++)
			in[i] = p[j * 16 + i];
		keyExpansion();
		cipher(in);
		for(unsigned int i = 0; i < 16; i ++)
			p[j * 16 + i] = in[i];
	}
	return p;
}
void AES::cipher(unsigned char in[16])
{
	for(unsigned int i=0;i<4;i++)
		for(unsigned int j=0;j<4;j++)
			_state[j][i] = in[i*4 + j];
	addRoundKey(0);
	for(unsigned int round=1;round<_nr;round++)
	{
		subBytes();
		shiftRows();
		mixColumns();
		addRoundKey(round);
	}
	subBytes();
	shiftRows();
	addRoundKey(_nr);
	for(unsigned int i=0;i<4;i++)
		for(unsigned int j=0;j<4;j++)
			in[i*4+j]=_state[j][i];
}

std::string AES::decrypt(std::string p)
{
	while(p.length()%16)
		p.push_back('\0');
	unsigned char in[16];
	for(unsigned int j = 0; j < (p.length() / 16); j ++)
	{
		for(unsigned int i = 0; i < 16; i ++)
			in[i] = p[j * 16 + i];
		keyExpansion();
		invCipher(in);
		for(unsigned int i = 0; i < 16; i ++)
			p[j * 16 + i] = in[i];
	}
	return p;
}
void AES::invCipher(unsigned char in[16])
{
	for(unsigned int i=0;i<4;i++)
		for(unsigned int j=0;j<4;j++)
			_state[j][i] = in[i*4 + j];
	addRoundKey(_nr);
	for(unsigned int round=_nr-1;round>0;round--)
	{
		invShiftRows();
		invSubBytes();
		addRoundKey(round);
		invMixColumns();
	}
	invShiftRows();
	invSubBytes();
	addRoundKey(0);
	for(unsigned int i=0;i<4;i++)
		for(unsigned int j=0;j<4;j++)
			in[i*4+j]=_state[j][i];
}

void AES::keyExpansion()
{
	unsigned int i,j;
	unsigned char temp[4],k;
	for(i=0;i<_nk;i++)
	{
		_round_key[i*4]=_key[i*4];
		_round_key[i*4+1]=_key[i*4+1];
		_round_key[i*4+2]=_key[i*4+2];
		_round_key[i*4+3]=_key[i*4+3];
	}
	while (i < (4 * (_nr+1)))
	{
		for(j=0;j<4;j++)
			temp[j]=_round_key[(i-1) * 4 + j];
		if (i % _nk == 0)
		{
			k = temp[0];
			temp[0] = temp[1];
			temp[1] = temp[2];
			temp[2] = temp[3];
			temp[3] = k;

			temp[0]=getSBoxValue(temp[0]);
			temp[1]=getSBoxValue(temp[1]);
			temp[2]=getSBoxValue(temp[2]);
			temp[3]=getSBoxValue(temp[3]);

			temp[0] =  temp[0] ^ _rcon[i/_nk];
		}
		else if (_nk > 6 && i % _nk == 4)
		{
			temp[0]=getSBoxValue(temp[0]);
			temp[1]=getSBoxValue(temp[1]);
			temp[2]=getSBoxValue(temp[2]);
			temp[3]=getSBoxValue(temp[3]);
		}
		_round_key[i*4+0] = _round_key[(i-_nk)*4+0] ^ temp[0];
		_round_key[i*4+1] = _round_key[(i-_nk)*4+1] ^ temp[1];
		_round_key[i*4+2] = _round_key[(i-_nk)*4+2] ^ temp[2];
		_round_key[i*4+3] = _round_key[(i-_nk)*4+3] ^ temp[3];
		i++;
	}
}
void AES::addRoundKey(int round)
{
	for(unsigned int i=0;i<4;i++)
		for(unsigned int j=0;j<4;j++)
			_state[j][i] ^= _round_key[round * 16 + i * 4 + j];
}

void AES::subBytes()
{
	for(unsigned int i=0;i<4;i++)
		for(unsigned int j=0;j<4;j++)
			_state[i][j] = getSBoxValue(_state[i][j]);
}
void AES::invSubBytes()
{
	for(unsigned int i=0;i<4;i++)
		for(unsigned int j=0;j<4;j++)
			_state[i][j] = getSBoxInvert(_state[i][j]);
}

void AES::shiftRows()
{
	unsigned char temp;
	temp=_state[1][0];
	_state[1][0]=_state[1][1];
	_state[1][1]=_state[1][2];
	_state[1][2]=_state[1][3];
	_state[1][3]=temp;

	temp=_state[2][0];
	_state[2][0]=_state[2][2];
	_state[2][2]=temp;

	temp=_state[2][1];
	_state[2][1]=_state[2][3];
	_state[2][3]=temp;

	temp=_state[3][0];
	_state[3][0]=_state[3][3];
	_state[3][3]=_state[3][2];
	_state[3][2]=_state[3][1];
	_state[3][1]=temp;
}
void AES::invShiftRows()
{
	unsigned char temp;

	temp=_state[1][3];
	_state[1][3]=_state[1][2];
	_state[1][2]=_state[1][1];
	_state[1][1]=_state[1][0];
	_state[1][0]=temp;

	temp=_state[2][0];
	_state[2][0]=_state[2][2];
	_state[2][2]=temp;

	temp=_state[2][1];
	_state[2][1]=_state[2][3];
	_state[2][3]=temp;

	temp=_state[3][0];
	_state[3][0]=_state[3][1];
	_state[3][1]=_state[3][2];
	_state[3][2]=_state[3][3];
	_state[3][3]=temp;
}

void AES::mixColumns()
{
	unsigned char Tmp,Tm,t;
	for(unsigned int i=0;i<4;i++)
	{
		t=_state[0][i];
		Tmp = _state[0][i] ^ _state[1][i] ^ _state[2][i] ^ _state[3][i];
		Tm = _state[0][i] ^ _state[1][i];
		Tm = xtime(Tm);
		_state[0][i] ^= Tm ^ Tmp;
		Tm = _state[1][i] ^ _state[2][i];
		Tm = xtime(Tm);
		_state[1][i] ^= Tm ^ Tmp;
		Tm = _state[2][i] ^ _state[3][i];
		Tm = xtime(Tm);
		_state[2][i] ^= Tm ^ Tmp;
		Tm = _state[3][i] ^ t;
		Tm = xtime(Tm);
		_state[3][i] ^= Tm ^ Tmp;
	}
}
void AES::invMixColumns()
{
	unsigned char a,b,c,d;
	for(unsigned int i=0;i<4;i++)
	{
		a = _state[0][i];
		b = _state[1][i];
		c = _state[2][i];
		d = _state[3][i];

		_state[0][i] = Multiply(a, 0x0e) ^ Multiply(b, 0x0b) ^ Multiply(c, 0x0d) ^ Multiply(d, 0x09);
		_state[1][i] = Multiply(a, 0x09) ^ Multiply(b, 0x0e) ^ Multiply(c, 0x0b) ^ Multiply(d, 0x0d);
		_state[2][i] = Multiply(a, 0x0d) ^ Multiply(b, 0x09) ^ Multiply(c, 0x0e) ^ Multiply(d, 0x0b);
		_state[3][i] = Multiply(a, 0x0b) ^ Multiply(b, 0x0d) ^ Multiply(c, 0x09) ^ Multiply(d, 0x0e);
	}
}

int AES::getSBoxValue(int num)
{
	return _sbox[num];
}
int AES::getSBoxInvert(int num)
{
	return _rsbox[num];
}
std::string AES::getKey()
{
	return _key;
}

void AES::setKey(std::string key)
{
	_key = key;
}
