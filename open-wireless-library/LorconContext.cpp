#include "LorconContext.h"
/****************************************************************************************
 * LorconContext																		*
 * The porpose of this class is to wrap the lorcon struct.								*
 * while lorcon's headers provide tools to manage 802.11 communication in C language,	*
 * this class provides tools for C++.													*
 ****************************************************************************************/

LorconContext::LorconContext(ThreadManager *tm, std::string interface)
{
	//	choose driver to capture on:
	lorcon_driver_t *_dri = lorcon_auto_driver(interface.c_str());
	if (_dri == NULL)
	{
		Log::E("Couldn't detect driver or no supported driver for ");
		_ctx = NULL;
        return;
	}
	//	create a lorcon context:
	else if ((_ctx = lorcon_create(interface.c_str(), _dri)) == NULL)
	{
		Log::E("Failed to create LORCON context for " + interface + " " + _dri->name);
		lorcon_free_driver_list(_dri);
        return;
	}
	else
	{
		lorcon_free_driver_list(_dri);
		//	open the context for injection and monitor mode:
		if (lorcon_open_injmon(_ctx) < 0)
		{
			std::string s("Failed to open injmon: ");
            s += lorcon_get_error(_ctx);
			Log::E(s);
			lorcon_free(_ctx);
		}
	}

	lorcon_set_timeout(_ctx, 100);
	//	save a pointer to the thread manager in the context. will be used in the clb function:
	_ctx->auxptr = tm;
}
LorconContext::~LorconContext()
{
	lorcon_breakloop(_ctx);
	if(_ctx != NULL)
		lorcon_free(_ctx);
}

//	sends a string:
bool LorconContext::send(std::string packet)
{
	if(_ctx == NULL)
		return false;
	if (lorcon_send_bytes(_ctx, packet.length(), (u_char*)packet.c_str()) < 0)//lorcon sends the packet...
	{
        Log::E(lorcon_get_error(_ctx));
		return false;
	}
	return true;
}

//	just sniffing around...
void LorconContext::sniff()
{
	Log::D("now sniffing...");
	lorcon_loop(_ctx, 0, LorconContext::clb, NULL);//sniff packet and handle them with the callback function
}

//	every packet captured by the sniff function goes here:
void LorconContext::clb(lorcon_t *con, lorcon_packet_t *p, u_char *user)
{
	ThreadManager *tm = (ThreadManager *)con->auxptr;
	std::string str = "", app_id = tm->getAppID(), serial;

	//convert the packet to string:
	for(int i=0; i<p->length; i++)
		str.push_back(p->packet_raw[i]);

	//make sure the packet belongs to OWL protocol and to the specific app that uses it:
	if(str.find(app_id) != std::string::npos)
	{
		Message msg = Message::fromString(str.substr(str.find(app_id)+6));//remove the app id from the string
		if( (msg.getDst().getMac() == tm->getMac()) || ( (msg.getDst().getMac() == Message::BROADCAST) && (msg.getSrc().getMac() != tm->getMac()) ) )
			tm->push_exec(msg);
		else if((tm->getMode() == Manager::REPEATER) || (tm->getMode() == Manager::OWL_REPEATER))
			tm->_context->send(std::string("((((" + str.substr(str.find(app_id))));
	}
	else if(tm->getMode() == Manager::REPEATER)
		tm->_context->send(std::string("((((" + str.substr(str.find(app_id))));

}
