#pragma once
#include <iostream>
#include <mutex>
#include <map>

/****************************************************************
 * Log															*
 * a log that prints comments, errors and events to the screen	*
 * the log also provides information about						*
 * the time and thread on which the event took place.			*
 ****************************************************************/

class Log
{
public:
	static const char ALL = 0b11111111;
	static const char DBG = 0b00000001;
	static const char ERR = 0b00000010;
	static void E(std::string msg);
	static void D(std::string msg);
    static void setModel(char mod);
	virtual ~Log();

private:
	Log();
    static std::mutex	_mu;
	static int			_i;
	static char			_mod;
	static short		threadIndex;
	static short getThread();
	static std::map<long, short> threads; //map of all the thread<Tid, index>
	static const std::string currentDateTime();
};
