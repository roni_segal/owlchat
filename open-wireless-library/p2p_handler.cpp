#include "p2p_handler.h"

/************************************************************************************************************
 * p2p_handler																								*
 * this class organizes the communication with a specific device											*
 * this class is responsible for forwarding the outgoing messages to the ThreadManager,						*
 * handling incoming messages,																				*
 * encrypt/decrypt outgoing/incoming messages,																*
 * make sure there is no packet loss (by using sequence numbers, save sent messages until an ACK arrives...)*
 ************************************************************************************************************/

//constructor and destructor:
p2p_handler::p2p_handler(Device partner)
{
	_partner = new Device(partner);
	_tm = &ThreadManager::getThreadManager(&Manager::getManager());
	_key = NULL;
	setAESKey(partner.getAESKey());
	setInSeq(1);
	setOutSeq(1);
}
p2p_handler::~p2p_handler()
{
	if(_partner)
		delete _partner;
	if(_key)
		delete _key;
}

//getters:
Device p2p_handler::getPartner()
{
	return *_partner;
}
unsigned int p2p_handler::getInSeq()
{
	return _in_seq;
}
unsigned int p2p_handler::getOutSeq()
{
	return _out_seq;
}
std::mutex& p2p_handler::getMutex()
{
	return _buffer_mu;
}

//setters:
void p2p_handler::setInSeq(unsigned int num)
{
	_in_seq = num;
}
void p2p_handler::setOutSeq(unsigned int num)
{
	_out_seq = num;
}
void p2p_handler::generateAESKey()
{
	if(_key)
		delete _key;
	_key = new AES;
}
void p2p_handler::setAESKey(std::string k)
{
	if(_key)
		_key->setKey(k);
	else
		_key = new AES(k);
}

//peer to peer functions:
int p2p_handler::send(Message m)
{
	Message partialMsg;
	if(m.getData().length() > Manager::WINDOW_SIZE)
	{
		while(m.getData().length() > Manager::WINDOW_SIZE)
		{
			partialMsg.setType(Message::PARTIAL | Message::DATA);
			partialMsg.setData(m.getData().substr(0, Manager::WINDOW_SIZE));
			send(partialMsg);
			m.setData(m.getData().substr(Manager::WINDOW_SIZE));
		}
		m.setType(Message::PARTIAL | Message::DATA |Message::FIN);
	}


	m.encrypt(*_key);
	m.setDst(*_partner);

	std::unique_lock<std::mutex> lk(_ack_mu);
	_cv_ack.wait(lk, [this]{return this->_ack_buffer.size() < 1000;});

	if(!_ack_buffer.size())
	{
		m.setSeq(_out_seq);
		setOutSeq(_out_seq + 1);
		_tm->send(m);
	}
	this->_ack_buffer.push(m);

	lk.unlock();
	return 0;
}
Message p2p_handler::recv()
{
	Message m;
	std::unique_lock<std::mutex> lk(_buffer_mu);
	_cv_buf.wait(lk, [this](){return _buffer.size();});
	m = _buffer.front();
	_buffer.pop();
	lk.unlock();

	while(m.getType() == (Message::DATA | Message::PARTIAL))
	{
		lk.lock();
		_cv_buf.wait(lk, [this](){return _buffer.size();});
		m.setData(m.getData() + _buffer.front().getData());
		m.setType(_buffer.front().getType());
		_buffer.pop();
		lk.unlock();
	}
	_cv_buf.notify_all();
	m.setType(Message::DATA);
	return m;
}

bool p2p_handler::isConnected()
{
	return _connected == 2;
}

void p2p_handler::handle(Message m)
{
	unsigned char t = m.getType();
	if(t == Message::SYN)
		this->_connected = 1;
	else if(t == Message::ACK)
		this->_connected = 2;
	else if(t == (Message::DATA | Message::ACK))
	{
		//TODO: make sure the ack is real using the decrypted data's md5 (send it with the ack)
		_ack_mu.lock();
		if(_ack_buffer.front().getSeq() == m.getSeq())
		{
			_ack_buffer.pop();
			if(_ack_buffer.size())
			{
				_ack_buffer.front().setSeq(_out_seq);
				setOutSeq(_out_seq + 1);
				_tm->send(_ack_buffer.front());
			}
		}
		else
			Log::D("bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzt");
		_ack_mu.unlock();
		_cv_ack.notify_all();
	}
	else if(t == Message::ERR1)
	{
		_ack_mu.lock();
		if(m.getSeq() == _ack_buffer.front().getSeq())
			_tm->send(_ack_buffer.front());
		_ack_mu.unlock();
	}
	else
	{
		if(m.getSeq() == _in_seq)
		{
			sendDataAck();
			//setInSeq(_in_seq + 1);
			m.decrypt(*_key);
			if(t == Message::DATA || (t & (Message::DATA | Message::PARTIAL)))
			{
				//this->_buffer_mu.lock();
				this->_buffer.push(m);
				//this->_buffer_mu.unlock();
				this->_cv_buf.notify_all();
			}
		}
		else if(m.getSeq() > _in_seq)
		{
			Message errm(*_partner, Message::ERR1, "");
			errm.setSeq(_in_seq);
			_tm->send(errm);
		}
	}
}

void p2p_handler::sendDataAck()
{
	Message ack_msg(*_partner, (Message::DATA | Message::ACK), "");
	ack_msg.setSeq(_in_seq);
	_tm->send(ack_msg);
	setInSeq(_in_seq + 1);
}
