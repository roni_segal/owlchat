#pragma once

#include<iostream>
#include<string>
#include<cstring>
#include<cstdlib>
#include<cmath>

typedef unsigned long int RSA_KEY;

class RSA
{
public:
	RSA();
	~RSA();

	std::string			encrypt(std::string s);
	std::string			decrypt(std::string s);
	static std::string	encrypt(std::string s, RSA_KEY n, unsigned int e);
	static std::string	decrypt(std::string s, RSA_KEY n, RSA_KEY d);
	RSA_KEY				getN(){return _n;};
	RSA_KEY				getE(){return _e;};
	RSA_KEY				getD(){return _d;};
private:
	static RSA_KEY		cipher(RSA_KEY c, RSA_KEY n, RSA_KEY e);
	bool				isPrime(RSA_KEY num);
	RSA_KEY				randomPrime(int n);
	int					generate();
	unsigned int		e();
	RSA_KEY				d();

	RSA_KEY			_phi;
	RSA_KEY			_n;
	RSA_KEY			_p;
	RSA_KEY			_q;
	unsigned int	_e;
	RSA_KEY			_d;
};
