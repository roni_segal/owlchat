#include "Manager.h"

/********************************************************************************************************************
 * Manager																											*
 * This class supposed to make the link between the OWL library and the developers using it.						*
 * The Manager class manages the communication with other devices													*
 * and provides the developer a simple interface allowing him to send and receive packets to/from other devices,	*
 * send and listen to broadcasts according to OWL protocol,															*
 * search for nearby devices that use the same OWL supporting application,											*
 * and many more cool options! (not really)																			*
 ********************************************************************************************************************/

//constructor and destructor:
Manager::Manager()
{
    this->_me = NULL;
	this->_me = Device::me();
	this->_mode = NORMAL;
	this->_thread_manager = &ThreadManager::getThreadManager(this);
	this->_bro_clbk = NULL;
	this->_p2p_clbk = NULL;
    this->_p2p_clbk_arg = (void*)NULL;
	this->_broadcast_handler = new broadcast_handler();
	RSA r;
	this->_rsa_d = r.getD();
	this->_rsa_e = r.getE();
	this->_rsa_n = r.getN();
}
Manager& Manager::getManager()
{
    static Manager man;
    return man;
}

Manager::~Manager()
{
	delete this->_me;
	delete this->_broadcast_handler;
	std::map<Device, p2p_handler*>::iterator it = this->_p2p_handlers.begin();
	for(; it != this->_p2p_handlers.end(); it ++)
		delete it->second;
}

//getters:
bro_clbk Manager::getBroadcastCallback()
{
	return this->_bro_clbk;
}
p2p_clbk Manager::getP2PCallback()
{
	return this->_p2p_clbk;
}

void* Manager::getP2PCallbackArg()
{
    return this->_p2p_clbk_arg;
}

std::string Manager::getName()
{
	return this->_me->getName();
}
std::string Manager::getMac()
{
	return _me->getMac();
}
std::string Manager::getAppID()
{
	return std::string(this->_app_id);
}

//setters:
void Manager::setBroadcastCallback(bro_clbk clbk)
{
	this->_bro_clbk = clbk;
}
void Manager::setP2PCallback(p2p_clbk clbk, void* arg)
{
	this->_p2p_clbk = clbk;
    this->_p2p_clbk_arg = arg;
}
bool Manager::setName(std::string name)
{
	//A-Za-z0-9
	for(unsigned int i = 0; i < name.length(); i++)
		if(!((name[i] > 'A' && name[i] < 'Z') || (name[i] > 'a' && name[i] < 'z') || (name[i] > '0' && name[i] < '9')))
			return false;
	this->_me->setName(name);
	return true;
}
void Manager::setAppID(std::string id)
{
	for(int i = 0; i < 6; i++)
		this->_app_id[i] = id[i];
}

//send string:
int Manager::send_raw(std::string msg_str)
{
	return 0; //Deprecated
}

//peer to peer:
int Manager::send(Device device, Message msg)
{
	if(this->_p2p_handlers.find(device) == this->_p2p_handlers.end())
		return 0;
	p2p_handler* handler = this->_p2p_handlers[device];
	msg.setSrc(*_me);
	handler->send(msg);
	return 1;
}
Message Manager::recv(Device device)
{
	if(this->_p2p_handlers.find(device) == this->_p2p_handlers.end())
		return Message();
	p2p_handler* handler = this->_p2p_handlers.find(device)->second;
	Message result = handler->recv();
	result.setSrc(*(_known_devs.find(result.getSrc())));

	return result;
}
int Manager::connect(Device device)
{
	if(_known_devs.find(device) == _known_devs.end())
		return 0;
	Device d = *_known_devs.find(device);
	if(this->_p2p_handlers.find(device) != this->_p2p_handlers.end())
		return 0;
	AES a;
	d.setAESKey(a.getKey());
	_known_devs.erase(d);
	_known_devs.insert(d);
	std::string key =  RSA::encrypt(a.getKey(), d.getN(), d.getE());
	this->_thread_manager->send(Message(device, Message::SYN, key));
	return 1;
}
int Manager::sendFile(Device device, std::string name)
{
	FILE *exein;
	exein = fopen("/home/drsudo7/Desktop/project_stuff/owl/owl", "rb");
	if (exein == NULL)
	{
		Log::D("could not open");
		return -1;
	}
	int gk;
	std::string str = "";
	Log::D("start sending");
	while ((gk = fgetc(exein)) != EOF)
	{
		str.push_back(gk);
		if(str.length() == 3000)
		{
			send(device, Message(device, Message::DATA, str));
			str = "";
		}
	}
	send(device, Message(device, Message::DATA, str));
	Log::D("file sent");
	return 1;
}

bool Manager::setmode(int mode)
{
	if((mode == NORMAL) || (mode == REPEATER) || (mode == OWL_REPEATER))
		_mode = mode;
	else
		return false;
	return true;
}
int Manager::getMode()
{
	return this->_mode;
}

//broadcast:
int Manager::broadcast(Message msg)
{
	this->_broadcast_handler->send(msg);
	return 1;
}
Message Manager::listen()
{
	return this->_broadcast_handler->recv();
}

std::set<Device> Manager::devicesLookup(unsigned int milliseconds)
{
	std::string s = "";
	for(unsigned int i = 0; i < sizeof(RSA_KEY); i++)
		s.push_back(*((unsigned char *)(&_rsa_n)+i));
	for(unsigned int i = 0; i < sizeof(RSA_KEY); i++)
		s.push_back(*((unsigned char *)(&_rsa_e)+i));
	if(milliseconds > 0)
	{
		this->_known_devs.clear();
		Message lookup_msg(Message::LOOKUP | Message::SYN, s+getName());
		broadcast(lookup_msg);
        usleep(milliseconds);
	}
	return this->_known_devs;
}

//pass each incoming packet to it's proper handler:
void Manager::handle(Message msg)
{
	std::string serial = msg.getSerial();
	if(std::find(_serials.begin(), _serials.end(), serial) != _serials.end())
		return;
	_serials.push_back(serial);
	if(_serials.size() > 30)
		_serials.pop_front();
	//save the device in d:
	Device *d = new Device(msg.getSrc());
	if(_known_devs.find(msg.getSrc()) != _known_devs.end())
	{
		delete d;
		d = new Device(*_known_devs.find(msg.getSrc()));
	}

	unsigned char type = msg.getType();

	/************************************************
	 *	handle each message according to its type:	*
	 *		-	broadcast data						*
	 *		-	device lookup						*
	 *		-	syn									*
	 *		-	syn ack								*
	 *		-	ack									*
	 *		-	key exchange error					*
	 ************************************************/
	if(msg.getDst() == Message::BROADCAST)
	{
		if(type == Message::DATA)
		{
			if(this->_known_devs.find(*d) != this->_known_devs.end())
				msg.setSrc(*this->_known_devs.find(*d));
			this->_broadcast_handler->handle(msg);//let the broadcast handler do the work
		}
		else if(type & Message::LOOKUP)
		{
			Log::D("Got lookup data: " + msg.getData().substr(sizeof(RSA_KEY)*2));

			//save information about the device:
			unsigned int key_len = sizeof(RSA_KEY) * 2;
			d->setPublicKey( msg.getData().substr(0, key_len) );
			d->setName( msg.getData().substr(key_len) );
			this->_known_devs.erase(*d);
			this->_known_devs.insert(*d);

			//if the message is a lookup request - send a response:
			if(type & Message::SYN)
			{
				//save the key in a string:
				std::string s = "";
				for(unsigned int i = 0; i < sizeof(RSA_KEY); i++)
					s.push_back(*((unsigned char *)(&_rsa_n)+i));
				for(unsigned int i = 0; i < sizeof(RSA_KEY); i++)
					s.push_back(*((unsigned char *)(&_rsa_e)+i));

				//send lookup ack:
				_thread_manager->send(Message(std::string(Message::BROADCAST), Message::LOOKUP | Message::ACK, s + getName()));
			}
		}
		return;
	}
	else if(!(type & Message::DATA)) //not a broadcast:
	{
		//syn:
		if((type == Message::SYN) && (_p2p_handlers.find(msg.getSrc()) == _p2p_handlers.end()))
		{
			//add the key to the device:
			AES a(RSA::decrypt(msg.getData(), _rsa_n, _rsa_d));
			d->setAESKey(a.getKey());
			_known_devs.erase(*d);
			_known_devs.insert(*d);

			//create the p2p_handler for the communication with the device:
			p2p_handler *handler = new p2p_handler(*d);
			handler->handle(msg);
			this->_p2p_handlers[msg.getSrc()] = handler;

			//send syn ack:
			_thread_manager->send(Message(msg.getSrc(), (Message::SYN | Message::ACK), md5(a.getKey())));

			Log::D("Name: " + (d->getName()));
			return;
		}
		//syn|ack:
		else if((type == (Message::SYN | Message::ACK)) && getP2PCallback() != nullptr)
		{
			if(msg.getData() == md5(_known_devs.find(*d)->getAESKey()))
			{
				//send ack:
				_thread_manager->send(Message(msg.getSrc(), Message::ACK, ""));

				Log::D("Name: " + (d->getName()));

				//create a p2p_handler:
				p2p_handler *handler = new p2p_handler(*d);
				this->_p2p_handlers[msg.getSrc()] = handler;

				//start the callback thread:
                std::thread t(getP2PCallback(), msg.getSrc(), getP2PCallbackArg());
				t.detach();
			}
			else
				_thread_manager->send(Message(msg.getSrc(), (Message::ERR1 | Message::ACK), ""));
			return;
		}
		//ack:
		else if(type == Message::ACK)
		{
			Log::D("Name: " + (d->getName()));

			//start the callback thread:
            std::thread t(getP2PCallback(), msg.getSrc(), getP2PCallbackArg());
			t.detach();

			//call the p2p_handler:
			this->_p2p_handlers[msg.getSrc()]->handle(msg);
			return;
		}
		//connection error:
		else if(type == (Message::ERR1 | Message::ACK))
		{
			Log::D("ack err");
			return;
			//TODO: send the rsa public key and wait for a "syn"
		}
	}
	this->_p2p_handlers[msg.getSrc()]->handle(msg);
	delete d;

}
