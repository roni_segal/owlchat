#pragma once

#include <iostream>
#include <fstream>
#include <mutex>
#include <thread>
#include <queue>
#include <condition_variable>
#include <cstdlib>
#include <ctime>
#include "Message.h"
#include "Manager.h"
#include "LorconContext.h"

class LorconContext;
class Manager;

class ThreadManager
{
private:
	std::queue<Message>* _l_exec_q;
	std::mutex* l_exec_mu;

	std::queue<Message>* _send_q;
	std::mutex* _send_mu;

	std::condition_variable* _cv_send;
	std::condition_variable* _cv_exec;

	std::thread* _listener_t;
	std::thread* _exec_t;
	std::thread* _sender_t;
	//std::thread* _ack_sender_t;
	Manager *_man;

	std::map<std::queue<Message>*, std::mutex*>* _send_buffers;
	std::mutex* _send_buf_mu;

	ThreadManager(Manager *m);
public:
    LorconContext*	_context;

	static ThreadManager& getThreadManager(Manager *m);
	void listener();
	void executioner();
	void sender();
	void ack_sender();
	void sandbox(Message msg);
	void send(Message msg);
	void push_exec(Message msg);
	std::string getAppID();
	std::string getMac();
	void registerSendBuffer(std::queue<Message>*, std::mutex*);
	int getMode();

	~ThreadManager();
};

