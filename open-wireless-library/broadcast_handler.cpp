#include "broadcast_handler.h"

/************************************************************************
 * broadcast_handler													*
 * this class organizes the communication through broadcast messages	*
 * broadcast messages aren't encrypted and packet loss is possible... :(*
 ************************************************************************/

//constructor and destructor:
broadcast_handler::broadcast_handler()
{
	this->_buffer = new std::queue<Message>;
	this->_buffer_mu = new std::mutex();
	this->_cv = new std::condition_variable();
}
broadcast_handler::~broadcast_handler()
{
	delete _buffer;
	delete _buffer_mu;
	delete _cv;
}

void broadcast_handler::send(Message msg)
{
	ThreadManager::getThreadManager(&Manager::getManager()).send(msg);
}
Message broadcast_handler::recv()
{
	if (this->_buffer->size() > 0)
	{
		Message msg = this->_buffer->front();
		this->_buffer->pop();
		return msg;
	}
	std::unique_lock<std::mutex> lk(*_buffer_mu);
	this->_cv->wait(lk, [this] {return  this->_buffer->size() > 0;});
	Message msg = this->_buffer->front();
	this->_buffer->pop();
	lk.unlock();
	return msg;
}

void broadcast_handler::handle(Message msg)
{
	this->_buffer_mu->lock();
	this->_buffer->push(msg);
	this->_buffer_mu->unlock();
	this->_cv->notify_all();
}
