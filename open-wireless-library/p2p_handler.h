#pragma once

#include<iostream>
#include <cstdio>
#include<queue>
#include<thread>
#include<mutex>
#include <condition_variable>
#include "Device.h"
#include "AES.h"
#include "Message.h"
#include "Manager.h"
#include "ThreadManager.h"

/************************************************************************************************************
 * p2p_handler																								*
 * this class organizes the communication with a specific device											*
 * this class is responsible for forwarding the outgoing messages to the ThreadManager,						*
 * handling incoming messages,																				*
 * encrypt/decrypt outgoing/incoming messages,																*
 * make sure there is no packet loss (by using sequence numbers, save sent messages until an ACK arrives...)*
 ************************************************************************************************************/


class ThreadManager;

class p2p_handler
{
private:
	int 					_connected = 0;
	Device*					_partner;
	AES*					_key;
	unsigned int			_in_seq;
	unsigned int			_out_seq;
	std::queue<Message>		_buffer;
	std::queue<Message>		_ack_buffer;
	std::mutex				_buffer_mu;
	std::condition_variable	_cv_buf;
	std::mutex				_ack_mu;
	std::condition_variable	_cv_ack;
	std::string				_file;
	ThreadManager*			_tm;

	void sendDataAck();

public:
	//constructor and destructor:
	p2p_handler(Device partner);
	~p2p_handler();

	//getters:
	Device			getPartner();
	unsigned int	getInSeq();
	unsigned int	getOutSeq();
	std::mutex&		getMutex();

	//setters:
	void	setInSeq(unsigned int num);
	void	setOutSeq(unsigned int num);
	void	generateAESKey();
	void	setAESKey(std::string k);

	//peer to peer functions:
	int		send(Message m);
	Message	recv();
	void	handle(Message m);

	bool isConnected();
};

