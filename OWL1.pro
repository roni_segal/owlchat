#-------------------------------------------------
#
# Project created by QtCreator 2016-05-19T06:01:30
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OWL1
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    chattab.cpp \
    open-wireless-library/Device.cpp \
    open-wireless-library/RSA.cpp \
    open-wireless-library/ThreadManager.cpp \
    open-wireless-library/source.cpp \
    open-wireless-library/p2p_handler.cpp \
    open-wireless-library/Message.cpp \
    open-wireless-library/MD5.cpp \
    open-wireless-library/Manager.cpp \
    open-wireless-library/LorconContext.cpp \
    open-wireless-library/Log.cpp \
    open-wireless-library/broadcast_handler.cpp \
    open-wireless-library/AES.cpp

HEADERS  += mainwindow.h \
    chattab.h \
    open-wireless-library/Device.h \
    open-wireless-library/RSA.h \
    open-wireless-library/ThreadManager.h \
    open-wireless-library/p2p_handler.h \
    open-wireless-library/Message.h \
    open-wireless-library/MD5.h \
    open-wireless-library/Manager.h \
    open-wireless-library/LorconContext.h \
    open-wireless-library/Log.h \
    open-wireless-library/broadcast_handler.h \
    open-wireless-library/AES.h

FORMS    += mainwindow.ui

CONFIG += c++11
LIBS += -lorcon2


DISTFILES += \
    open-wireless-library/eclipse-configs.txt~ \
    open-wireless-library/eclipse-configs.txt

